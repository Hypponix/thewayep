# TheWayEP

You'll find the [**design of the box**](https://gitlab.com/Hypponix/thewayep/blob/master/Box_EP_The_Way.svg) I created for the EP The Way and, soon, the ableton files so if you want to have fun and remix the song, you can.
The songs are available under the [**Creative Commons**](https://creativecommons.org/share-your-work/licensing-types-examples/) license CC-BY-SA on [**SoundCloud**](https://soundcloud.com/hypponix/sets/ep-the-way-hypponix).

I hope you enjoy it.

Ps : feel free to ask any question about this git if you have some.
